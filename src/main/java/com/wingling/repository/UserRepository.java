package com.wingling.repository;

import com.wingling.model.User;
import org.springframework.stereotype.Repository;

/**
 * @author: zhangocean
 * @Date: 2018/9/8 15:41
 * Describe:
 */
@Repository
public interface UserRepository {

    /**
     *  通过手机号查找用户
     * @param phone 手机号
     * @return 用户信息
     */
    User findByPhone(String phone);

}
