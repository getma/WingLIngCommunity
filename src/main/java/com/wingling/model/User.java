package com.wingling.model;

import lombok.Data;

import java.util.List;

/**
 * @author: zhangocean
 * @Date: 2018/9/8 15:28
 * Describe:
 */
@Data
public class User {

    private int id;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 学号
     */
    private long schoolNumber;

    /**
     * 性别
     */
    private String gender;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 最后活跃时间
     */
    private String recentlyLanded;

    /**
     * 头像地址
     */
    private String avatarImgUrl;

    private List<Role> roles;

}
