package com.wingling.filters;

import com.alibaba.druid.support.http.WebStatFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author: zhangocean
 * @Date: 2018/9/8 15:06
 * Describe: 查看数据源及SQL统计配置
 */
@WebFilter(filterName = "druidWebStatFilter", urlPatterns = "/*",
            initParams = {
                    @WebInitParam(name = "exclusions", value = "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")
            })
public class DruidStatFilter extends WebStatFilter {
}
