package com.wingling.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author: zhangocean
 * @Date: 2018/9/8 13:35
 * Describe:
 */
@Controller
public class IndexControl {

    @GetMapping("/")
    public String index(){
        return "index";
    }

}
