package com.wingling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author: zhangocean
 * @Date: 2018/9/8 13:35
 * Describe: SpringBoot启动入口
 */
@SpringBootApplication
@ServletComponentScan
public class WingLingCommunityApplication {

	public static void main(String[] args) {
		SpringApplication.run(WingLingCommunityApplication.class, args);
	}
}
