package com.wingling.zhy;

import net.sf.json.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

/**
 * @author: zhangocean
 * @Date: 2018/9/8 15:00
 * Describe:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DataSourceTests {

    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void testDataSource(){
        DataSource dataSource = applicationContext.getBean(DataSource.class);

        System.out.println(dataSource.getClass().getName());
    }

}
